import torch
from torch import nn
import pytorch_lightning as pl
import itertools

from networks import *
from util import *
from losses import *


class UnetVAE(pl.LightningModule):
        
    def __init__(
        self,
        markers = [],
        G_lr = 0.002,
        D_lr = 0.002,
        GAN_mode = 'vanilla',
        ngf_pow = 7,
        ndf = 64,
        n_layers = 3,
        lambda_kl_HE = 0.1,
        lambda_kl_IF = 0.1,
        lambda_rec_HE = 100,
        lambda_rec_IF = 100,
        lambda_GAN_rec_HE = 1,
        lambda_GAN_rec_IF = 1,
        lambda_me = 1,
        lambda_pred_HE2IF = 100,
        lambda_pred_IF2HE = 100,
        lambda_GAN_pred_HE2IF = 1,
        lambda_GAN_pred_IF2HE = 1,
        lambda_kl_HE2IF = 0.1,
        lambda_kl_IF2HE = 0.1,        
        lambda_cyc_HE2IF2HE = 100,
        lambda_cyc_IF2HE2IF = 100,
    ):
        """
        Args:
            markers: list of IF markers in experiment
            G_lr: generator (encoder + decoder) learning rate
            D_lr: discriminator learning rate
            GAN_mode: 
        """

        super(UnetVAE, self).__init__()

        self.save_hyperparameters()
        
        n_markers = len(markers)
        
        assert n_markers > 0
        
        shared_enc = ResidualBlock(2**(self.hparams.ngf_pow+3))
        self.HE_encoder = UnetEncoder(in_channels=3, 
                                      shared_block=shared_enc, 
                                      ngf_pow=self.hparams.ngf_pow).apply(weights_init_normal)
        self.IF_encoder = UnetEncoder(in_channels=n_markers, 
                                      shared_block=shared_enc,
                                      ngf_pow=self.hparams.ngf_pow).apply(weights_init_normal)
        
        shared_dec = ResidualBlock(2**(self.hparams.ngf_pow+3))
        self.IF_decoder = UnetDecoder(out_channels=n_markers, 
                                      shared_block=shared_dec,
                                      ngf_pow=self.hparams.ngf_pow).apply(weights_init_normal)
        self.HE_decoder = UnetDecoder(out_channels=3, 
                                      shared_block=shared_dec,
                                      ngf_pow=self.hparams.ngf_pow).apply(weights_init_normal)
        
        self.HE_disc = NLayerDiscriminator(3 + n_markers, 
                                           ndf=self.hparams.ndf,
                                           n_layers=self.hparams.n_layers).apply(weights_init_normal)
        self.IF_disc = NLayerDiscriminator(3 + n_markers,
                                           ndf=self.hparams.ndf,
                                           n_layers=self.hparams.n_layers).apply(weights_init_normal)

        self.criterionGAN = GANLoss(self.hparams.GAN_mode)
        self.criterionL1 = nn.L1Loss()
        self.criterionME = nn.CosineEmbeddingLoss()

        
    def forward(self, x):
        _, z = self.HE_encoder(x)
        return self.HE_decoder(z, self.HE_encoder)

    
    def log_images(self, phase):
        # ground truth
        HE_in_grid = make_HE_grid(self.out['HE_in'])
        IF_in_grid = make_IF_grid(self.out['IF_in'])
        self.logger.experiment.add_image(f'{phase}_GT', np.dstack((HE_in_grid, IF_in_grid)), self.global_step)
        
        HE2IF_pred_grid = make_IF_grid(self.out['HE2IF_pred'])
        self.logger.experiment.add_image(f'{phase}_HE2IF_pred', HE2IF_pred_grid, self.global_step)
        
        IF2HE_pred_grid = make_HE_grid(self.out['IF2HE_pred'])
        self.logger.experiment.add_image(f'{phase}_IF2HE_pred', IF2HE_pred_grid, self.global_step)
        
        HE_rec_grid = make_HE_grid(self.out['HE_rec'])
        self.logger.experiment.add_image(f'{phase}_HE_rec', np.dstack((HE_in_grid, HE_rec_grid)), self.global_step)
        
        IF_rec_grid = make_IF_grid(self.out['IF_rec'])
        self.logger.experiment.add_image(f'{phase}_IF_rec', IF_rec_grid, self.global_step)
        
        HE2IF2HE_rec_grid = make_HE_grid(self.out['HE2IF2HE_rec'])
        self.logger.experiment.add_image(f'{phase}_HE2IF2HE_rec', HE2IF2HE_rec_grid, self.global_step)
        
        IF2HE2IF_rec_grid = make_IF_grid(self.out['IF2HE2IF_rec'])
        self.logger.experiment.add_image(f'{phase}_IF2HE2IF_rec', IF2HE2IF_rec_grid, self.global_step) 
        
        
    def _run_step(self, HE_in, IF_in):
        # reconstructions
        HE_mu, HE_z = self.HE_encoder(HE_in)
        HE_rec = self.HE_decoder(HE_z, self.HE_encoder)
        IF_mu, IF_z = self.IF_encoder(IF_in)
        IF_rec = self.IF_decoder(IF_z, self.IF_encoder)
        
        # domain translations
        HE2IF_pred = self.IF_decoder(HE_z, self.HE_encoder)
        IF2HE_pred = self.HE_decoder(IF_z, self.IF_encoder)
        
        # cycle reconstructions
        HE2IF_mu, HE2IF_z = self.IF_encoder(HE2IF_pred)
        HE2IF2HE_rec = self.HE_decoder(HE2IF_z, self.IF_encoder)
        IF2HE_mu, IF2HE_z = self.HE_encoder(IF2HE_pred)
        IF2HE2IF_rec = self.IF_decoder(IF2HE_z, self.HE_encoder)
        
        return {'HE_in' : HE_in,
                'IF_in' : IF_in,
                'HE_rec' : HE_rec,
                'IF_rec' : IF_rec,
                'HE_mu': HE_mu,
                'HE_z' : HE_z,
                'IF_mu' : IF_mu,
                'IF_z' : IF_z,
                'HE2IF_pred' : HE2IF_pred,
                'IF2HE_pred' : IF2HE_pred,
                'HE2IF_mu' : HE2IF_mu, 
                'HE2IF_z' : HE2IF_z,
                'IF2HE_mu' : IF2HE_mu, 
                'IF2HE_z' : IF2HE_z,
                'HE2IF2HE_rec' : HE2IF2HE_rec,
                'IF2HE2IF_rec' : IF2HE2IF_rec}

    
    def step(self, batch, batch_idx, optimizer_idx, phase):
        (optim_G, optim_D) = self.optimizers(use_pl_optimizer=True)
        
        HE_in, IF_in = batch
        self.out = self._run_step(HE_in, IF_in)
        
        # Ds require no gradients when optimizing Gs
        self.set_requires_grad([self.IF_disc, self.HE_disc], False)
        self.backward_G()
        
        logs = {
            'loss_kl_HE': self.loss_kl_HE,
            'loss_kl_IF': self.loss_kl_IF,
            'loss_rec_HE': self.loss_rec_HE,
            'loss_rec_IF': self.loss_rec_IF,
            'loss_GAN_rec_HE': self.loss_GAN_rec_HE,
            'loss_GAN_rec_IF': self.loss_GAN_rec_IF,
            'loss_me': self.loss_me,
            'loss_pred_HE2IF': self.loss_pred_HE2IF,
            'loss_pred_IF2HE': self.loss_pred_IF2HE,
            'loss_GAN_pred_HE2IF': self.loss_GAN_pred_HE2IF,
            'loss_GAN_pred_IF2HE': self.loss_GAN_pred_IF2HE,
            'loss_kl_HE2IF': self.loss_kl_HE2IF,
            'loss_kl_IF2HE': self.loss_kl_IF2HE,
            'loss_cyc_HE2IF2HE': self.loss_cyc_HE2IF2HE,
            'loss_cyc_IF2HE2IF': self.loss_cyc_IF2HE2IF,
            'loss_G_total': self.loss_G_total
        }
    
        if phase=='train':
            self.manual_backward(self.loss_G_total, optim_G)
            optim_G.step()
        
            self.set_requires_grad([self.IF_disc, self.HE_disc], True)
            self.backward_D()
            self.manual_backward(self.loss_D_total, optim_D)
            optim_D.step()
            
            logs['loss_D_total'] = self.loss_D_total
            
        if batch_idx % 20 == 0:
            self.log_images(phase)
        
        return self.loss_G_total, logs
    
    
    def compute_kl(self, mu):
        mu_2 = torch.pow(mu, 2)
        loss = torch.mean(mu_2)
        return loss
    
    
    def r1_reg(self,d_out, x_in):
        # zero-centered gradient penalty for real images
        batch_size = x_in.size(0)
        grad_dout = torch.autograd.grad(
            outputs=d_out.sum(), inputs=x_in,
            create_graph=True, retain_graph=True, only_inputs=True
        )[0]
        grad_dout2 = grad_dout.pow(2)
        assert(grad_dout2.size() == x_in.size())
        reg = 0.5 * grad_dout2.view(batch_size, -1).sum(1).mean(0)
        return reg
    
    
    def backward_G(self):
        
        ### VAE losses ###
        # are the latents normal?
        self.loss_kl_HE = self.compute_kl(self.out['HE_mu'])
        self.loss_kl_IF = self.compute_kl(self.out['IF_mu'])
        
        # does the reconstruction preserve content?
        self.loss_rec_HE = self.criterionL1(self.out['HE_rec'], self.out['HE_in'])
        self.loss_rec_IF = self.criterionL1(self.out['IF_rec'], self.out['IF_in'])
        
        # does the reconstruction look real?
        self.rec_HE_real_IF = torch.cat((self.out['HE_rec'], self.out['IF_in']), 1)
        self.loss_GAN_rec_HE = self.criterionGAN(self.HE_disc(self.rec_HE_real_IF), True)
        self.real_HE_rec_IF = torch.cat((self.out['HE_in'], self.out['IF_rec']), 1)
        self.loss_GAN_rec_IF = self.criterionGAN(self.IF_disc(self.real_HE_rec_IF), True)

        ### translation losses ###
        # are paired HE/IF mutually encoded?
        bsize = self.out['IF_mu'].size(0)
        self.target = torch.ones(bsize).type_as(self.out['IF_mu'])
        self.loss_me = self.criterionME(self.out['HE_mu'].view(bsize,-1), 
                                        self.out['IF_mu'].view(bsize,-1), 
                                        self.target)
        
        # does the translation represent ground truth content?
        self.loss_pred_HE2IF = self.criterionL1(self.out['HE2IF_pred'], self.out['IF_in'])
        self.loss_pred_IF2HE = self.criterionL1(self.out['IF2HE_pred'], self.out['HE_in'])

        # does the translation look real?
        self.real_HE_pred_IF = torch.cat((self.out['HE_in'], self.out['HE2IF_pred']), 1)
        self.loss_GAN_pred_HE2IF = self.criterionGAN(self.IF_disc(self.real_HE_pred_IF), True)
        self.pred_HE_real_IF = torch.cat((self.out['IF2HE_pred'], self.out['IF_in']), 1)
        self.loss_GAN_pred_IF2HE = self.criterionGAN(self.HE_disc(self.pred_HE_real_IF), True)
    
        ### cyclic losses ###
        # are the cyclic latents normal?
        self.loss_kl_HE2IF = self.compute_kl(self.out['HE2IF_mu'])
        self.loss_kl_IF2HE = self.compute_kl(self.out['IF2HE_mu'])
        
        # does the cyclic reconstruction preserve content?
        self.loss_cyc_HE2IF2HE = self.criterionL1(self.out['HE2IF2HE_rec'], self.out['HE_in'])
        self.loss_cyc_IF2HE2IF = self.criterionL1(self.out['IF2HE2IF_rec'], self.out['IF_in'])
        
        self.loss_G_total = self.loss_kl_HE * self.hparams.lambda_kl_HE + \
                            self.loss_kl_IF * self.hparams.lambda_kl_IF + \
                            self.loss_rec_HE * self.hparams.lambda_rec_HE + \
                            self.loss_rec_IF * self.hparams.lambda_rec_IF + \
                            self.loss_GAN_rec_HE * self.hparams.lambda_GAN_rec_HE + \
                            self.loss_GAN_rec_IF * self.hparams.lambda_GAN_rec_IF + \
                            self.loss_me * self.hparams.lambda_me + \
                            self.loss_pred_HE2IF * self.hparams.lambda_pred_HE2IF + \
                            self.loss_pred_IF2HE * self.hparams.lambda_pred_IF2HE + \
                            self.loss_GAN_pred_HE2IF * self.hparams.lambda_GAN_pred_HE2IF + \
                            self.loss_GAN_pred_IF2HE * self.hparams.lambda_GAN_pred_IF2HE + \
                            self.loss_kl_HE2IF * self.hparams.lambda_kl_HE2IF + \
                            self.loss_kl_IF2HE * self.hparams.lambda_kl_IF2HE + \
                            self.loss_cyc_HE2IF2HE * self.hparams.lambda_cyc_HE2IF2HE + \
                            self.loss_cyc_IF2HE2IF * self.hparams.lambda_cyc_IF2HE2IF
        
    
    def backward_D(self):
        """Calculate GAN loss for discriminators"""
        real_HE_real_IF = torch.cat((self.out['HE_in'],self.out['IF_in']),1)
        self.loss_D_HE = self.backward_D_basic(self.HE_disc, real_HE_real_IF, self.pred_HE_real_IF)
        self.loss_D_IF = self.backward_D_basic(self.IF_disc, real_HE_real_IF, self.real_HE_pred_IF)
        self.loss_D_total = self.loss_D_IF + self.loss_D_HE
        
        
    def backward_D_basic(self, netD, real, fake):
        # Real
        pred_real = netD(real) # add .requires_grad if using r1_reg
#         loss_reg = self.r1_reg(pred_real, real)

        loss_real = self.criterionGAN(pred_real, True)
        # Fake
        pred_fake = netD(fake.detach())
        loss_fake = self.criterionGAN(pred_fake, False)
        # Combined loss and calculate gradients
        loss = 0.5 * (loss_real + loss_fake) # + loss_reg
        return loss
    
    
    def set_requires_grad(self, nets, requires_grad=False):
        if not isinstance(nets, list):
            nets = [nets]
        for net in nets:
            if net is not None:
                for param in net.parameters():
                    param.requires_grad = requires_grad

                    
    def training_step(self, batch, batch_idx, optimizer_idx):
        loss, logs = self.step(batch, batch_idx, optimizer_idx, phase='train')
        self.log_dict({f"train_{k}": v for k, v in logs.items()}, on_step=True, on_epoch=False)
        return loss

    
    def validation_step(self, batch, batch_idx):
        loss, logs = self.step(batch, batch_idx, None, phase='val')
        self.log_dict({f"val_{k}": v for k, v in logs.items()}, sync_dist=True)
        return loss

    
    def configure_optimizers(self):
        optim_G = torch.optim.Adam(
            itertools.chain(
                self.HE_encoder.parameters(),
                self.IF_encoder.parameters(),
                self.HE_decoder.parameters(),
                self.IF_decoder.parameters()), 
            lr=self.hparams.G_lr,
        betas=(0.5,0.999))
        optim_D = torch.optim.Adam(
            itertools.chain(
                self.HE_disc.parameters(),
                self.IF_disc.parameters()),
            lr=self.hparams.D_lr,
        betas=(0.5,0.999))
        return [optim_G, optim_D], []