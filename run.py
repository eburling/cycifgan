import numpy
import torch
import pytorch_lightning as pl
from pytorch_lightning.loggers import TensorBoardLogger
from pytorch_lightning.callbacks import ModelCheckpoint

from data import MxDataModule
from model import UnetVAE

    
torch.manual_seed(0)

data_path = '/home/groups/graylab_share/Chin_Lab/ChinData/Exchange/SARDANA/AllNormalizedTiles/054'
expr_path = '/mnt/scratch/STARDANA'
# markers = ['DNA_16bit', 'Aortic_smooth_muscle_actin_16bit', 'Collagen_16bit', 'CD31_16bit', 'Pan-cytokeratin_16bit', 'CD45_16bit', 'CD4_16bit', 'CD8a_16bit', 'CD68_16bit']
markers = ['DNA_16bit', 'Pan-cytokeratin_16bit']
batch_size = 8
train_split = 0.8

data = MxDataModule(batch_size=batch_size,
                    data_path=data_path,
                    expr_path=expr_path,
                    markers=markers,
                    extract=True,
                    train_split=train_split)

model = UnetVAE(markers=markers,
                G_lr = 0.002,
                D_lr = 0.002,
                GAN_mode = 'vanilla',
                ngf_pow = 6,
                ndf = 64,
                n_layers = 3,
                lambda_kl_HE = 0,
                lambda_kl_IF = 0,
                lambda_rec_HE = 0,
                lambda_rec_IF = 0,
                lambda_GAN_rec_HE = 0,
                lambda_GAN_rec_IF = 0,
                lambda_me = 0,
                lambda_pred_HE2IF = 100,
                lambda_pred_IF2HE = 0,
                lambda_GAN_pred_HE2IF = 1,
                lambda_GAN_pred_IF2HE = 0,
                lambda_kl_HE2IF = 0,
                lambda_kl_IF2HE = 0,        
                lambda_cyc_HE2IF2HE = 0,
                lambda_cyc_IF2HE2IF = 0)

logger = TensorBoardLogger('tb_logs_SHIFTXAE', name='test')

checkpoint_callback = ModelCheckpoint(monitor='val_loss_pred_HE2IF')
trainer = pl.Trainer(gpus=4,
                     max_epochs=100,
                     automatic_optimization=False,
                     accelerator='ddp',
                     logger=logger,
                     log_every_n_steps=1,
                     callbacks=[checkpoint_callback])

trainer.fit(model, data)

trainer.save_checkpoint(f"test_all.ckpt")