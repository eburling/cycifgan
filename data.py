import random
import tarfile
import numpy as np
from glob import glob
from skimage.io import imread

import torch
from torchvision import transforms
from torch.utils.data import Dataset, DataLoader
import pytorch_lightning as pl

class MxDataset(Dataset):
    def __init__(self,
                 expr_path: str = '/mnt/scratch/STARDANA',
                 idx: list = [],
                 markers: list = [],
                 HE_transform = None,
                 IF_transform = None):
        
        super(MxDataset, self).__init__()
        self.expr_path = expr_path
        self.idx = idx
        self.markers = markers
        self.HE_transform = HE_transform
        self.IF_transform = IF_transform
        self.samples = self.make_dataset()
        
    def make_dataset(self):
        HE_fnames = sorted(glob(f'{self.expr_path}/HE/*.tif'))
        HE_fnames = [HE_fnames[i] for i in self.idx]
        IF_fnames = [sorted(glob(f'{self.expr_path}/{m}/*.tif')) for m in self.markers]
        IF_fnames = list(zip(*IF_fnames))
        IF_fnames = [IF_fnames[i] for i in self.idx]
        return list(zip(HE_fnames, IF_fnames))
    
    def stack_IF(self, IF_fnames):
        stack = [imread(i)[:,:,0].astype(np.float32) / 65535 for i in IF_fnames]
        return np.dstack(stack)
    
    def __getitem__(self, idx):
        HE_fname, IF_fnames = self.samples[idx]
        HE_img = imread(HE_fname)
        IF_img = self.stack_IF(IF_fnames)
        if self.HE_transform is not None:
            HE_img = self.HE_transform(HE_img)
        if self.IF_transform is not None:
            IF_img = self.IF_transform(IF_img)
        return HE_img, IF_img
    
    def __len__(self):
        return len(self.samples)


class MxDataModule(pl.LightningDataModule):
    def __init__(
        self, 
        batch_size: int, 
        data_path: str,
        expr_path: str,
        markers: list,
        extract: bool = True,
        seed: int = 42,
        train_split: float = 0.8
    ):
        super(MxDataModule, self).__init__()
        self.HE_path = f'{data_path}/HE.tar.gz'
        self.IF_paths = [f'{data_path}/{marker}.tar.gz' for marker in markers]
        self.batch_size = batch_size
        self.data_path = data_path
        self.expr_path = expr_path
        self.markers = markers
        self.extract = extract
        self.seed = seed
        self.train_split = train_split
        self.HE_transform = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.5, 0.5, 0.5],
                                 std=[0.5, 0.5, 0.5])
        ])
        self.IF_transform = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.5,]*len(markers),
                                 std=[0.5,]*len(markers))
        ])
        
    def prepare_data(self):
        # extract data
        if self.extract:
            with tarfile.open(self.HE_path,'r:gz') as HE_tar:
                print(f'Extracting H&E tiles from {self.HE_path}...')
                HE_tar.extractall(self.expr_path)
            for idx, IF_path in enumerate(self.IF_paths):
                print(f'Extracting {self.markers[idx]} tiles from {IF_path}...')
                with tarfile.open(IF_path,'r:gz') as IF_tar:
                    IF_tar.extractall(self.expr_path)
        
    def setup(self, stage=None):
        # build datasets
        assert len(self.markers) > 0
        random.seed(self.seed)
        size = len(glob(f'{self.expr_path}/HE/*.tif'))
        idx = list(np.arange(size))
        idx_train = random.sample(idx, int(size * self.train_split))
        idx_val = list(set(idx) - set(idx_train))
        self.data_train = MxDataset(expr_path=self.expr_path,
                                    idx=idx_train,
                                    markers=self.markers,
                                    HE_transform=self.HE_transform,
                                    IF_transform=self.IF_transform)
        print(f'train set size: {len(idx_train)}')
        self.data_val = MxDataset(expr_path=self.expr_path,
                                  idx=idx_val,
                                  markers=self.markers,
                                  HE_transform=self.HE_transform,
                                  IF_transform=self.IF_transform)
        print(f'val set size: {len(idx_val)}')

    def train_dataloader(self):
        return DataLoader(dataset=self.data_train,
                          batch_size=self.batch_size,
                          drop_last=True,
                          num_workers=4,
                          pin_memory=True,
                          shuffle=True)
    
    def val_dataloader(self):
        return DataLoader(dataset=self.data_val,
                          batch_size=self.batch_size,
                          drop_last=False,
                          num_workers=4,
                          pin_memory=True,
                          shuffle=False)   
    
    def test_dataloader(self):
        pass