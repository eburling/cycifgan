import torch
from torch import nn
import functools


def weights_init_normal(m):
    classname = m.__class__.__name__
    if classname.find("Conv") != -1:
        torch.nn.init.normal_(m.weight.data, 0.0, 0.02)
    elif classname.find("BatchNorm2d") != -1:
        torch.nn.init.normal_(m.weight.data, 1.0, 0.02)
        torch.nn.init.constant_(m.bias.data, 0.0)


class UNetDown(nn.Module):
    def __init__(self, in_size, out_size, normalize=True, dropout=0.0):
        super(UNetDown, self).__init__()
        layers = [nn.Conv2d(in_size, out_size, 4, 2, 1, bias=False)]
        if normalize:
            layers.append(nn.InstanceNorm2d(out_size))
        layers.append(nn.LeakyReLU(0.2))
        if dropout:
            layers.append(nn.Dropout(dropout))
        self.model = nn.Sequential(*layers)

    def forward(self, x):
        return self.model(x)


class UNetUp(nn.Module):
    def __init__(self, in_size, out_size, dropout=0.0):
        super(UNetUp, self).__init__()
        layers = [
            nn.ConvTranspose2d(in_size, out_size, 4, 2, 1, bias=False),
            nn.InstanceNorm2d(out_size),
            nn.ReLU(inplace=True),
        ]
        if dropout:
            layers.append(nn.Dropout(dropout))

        self.model = nn.Sequential(*layers)

    def forward(self, x, skip_input):
        x = self.model(x)
        x = torch.cat((x, skip_input), 1)
        return x

    
class UnetEncoder(nn.Module):
    def __init__(self, in_channels=3, dropout=0.0, ngf_pow=7, shared_block=None):
        super(UnetEncoder, self).__init__()
        self.down1 = UNetDown(in_channels, 2**ngf_pow, normalize=False)
        self.down2 = UNetDown(2**ngf_pow, 2**(ngf_pow+1))
        self.down3 = UNetDown(2**(ngf_pow+1), 2**(ngf_pow+2))
        self.down4 = UNetDown(2**(ngf_pow+2), 2**(ngf_pow+3), dropout=dropout)
        self.down5 = UNetDown(2**(ngf_pow+3), 2**(ngf_pow+3), dropout=dropout)
        self.down6 = UNetDown(2**(ngf_pow+3), 2**(ngf_pow+3), dropout=dropout)
        self.down7 = UNetDown(2**(ngf_pow+3), 2**(ngf_pow+3), normalize=False, dropout=dropout)

        self.shared_block = shared_block
        
    def reparameterization(self, mu):
        z = torch.randn(mu.shape).type_as(mu)
        return z + mu
    
    def forward(self, x):
        self.d1 = self.down1(x)
        self.d2 = self.down2(self.d1)
        self.d3 = self.down3(self.d2)
        self.d4 = self.down4(self.d3)
        self.d5 = self.down5(self.d4)
        self.d6 = self.down6(self.d5)
        self.d7 = self.down6(self.d6)
        self.mu = self.shared_block(self.d7)
        self.z = self.reparameterization(self.mu)
        return self.mu, self.z
    
        
class UnetDecoder(nn.Module):
    def __init__(self, out_channels=3, ngf_pow=7, shared_block=None):
        super(UnetDecoder, self).__init__()
        self.up1 = UNetUp(2**(ngf_pow+3), 2**(ngf_pow+3))
        self.up2 = UNetUp(2**(ngf_pow+4), 2**(ngf_pow+3))
        self.up3 = UNetUp(2**(ngf_pow+4), 2**(ngf_pow+3))
        self.up4 = UNetUp(2**(ngf_pow+4), 2**(ngf_pow+2))
        self.up5 = UNetUp(2**(ngf_pow+3), 2**(ngf_pow+1))
        self.up6 = UNetUp(2**(ngf_pow+2), 2**ngf_pow)
        self.final = nn.Sequential(
            nn.Upsample(scale_factor=2),
            nn.ZeroPad2d((1, 0, 1, 0)),
            nn.Conv2d(2**(ngf_pow+1), out_channels, 4, padding=1),
            nn.Tanh())
        
        self.shared_block = shared_block
        
    def forward(self, z, encoder):
        z = self.shared_block(z)
        u1 = self.up1(z, encoder.d6)
        u2 = self.up2(u1, encoder.d5)
        u3 = self.up3(u2, encoder.d4)
        u4 = self.up4(u3, encoder.d3)
        u5 = self.up5(u4, encoder.d2)
        u6 = self.up6(u5, encoder.d1)
        return self.final(u6)
    
    
class ResidualBlock(nn.Module):
    def __init__(self, features):
        super(ResidualBlock, self).__init__()

        conv_block = [
            nn.ReflectionPad2d(1),
            nn.Conv2d(features, features, 3),
            nn.InstanceNorm2d(features),
            nn.ReLU(inplace=True),
            nn.ReflectionPad2d(1),
            nn.Conv2d(features, features, 3),
            nn.InstanceNorm2d(features),
        ]

        self.conv_block = nn.Sequential(*conv_block)

    def forward(self, x):
        return x + self.conv_block(x)
    
    
class ResidualBlock1x1(nn.Module):
    def __init__(self, features):
        super(ResidualBlock1x1, self).__init__()

        conv_block = [
            nn.Conv2d(features, features, 1),
            nn.InstanceNorm2d(features),
            nn.ReLU(inplace=True),
            nn.Conv2d(features, features, 1),
            nn.InstanceNorm2d(features),
        ]

        self.conv_block = nn.Sequential(*conv_block)

    def forward(self, x):
        return x + self.conv_block(x)
    
    
class NLayerDiscriminator(nn.Module):
    """Defines a PatchGAN discriminator"""

    def __init__(self, 
                 input_nc, 
                 ndf=64, 
                 n_layers=3, 
                 norm_layer=nn.BatchNorm2d):
        """Construct a PatchGAN discriminator
        Parameters:
            input_nc (int)  -- the number of channels in input images
            ndf (int)       -- the number of filters in the last conv layer
            n_layers (int)  -- the number of conv layers in the discriminator
            norm_layer      -- normalization layer
        """
        super(NLayerDiscriminator, self).__init__()
        if type(norm_layer) == functools.partial:  # don't need bias as BatchNorm2d has affine parameters
            use_bias = norm_layer.func == nn.InstanceNorm2d
        else:
            use_bias = norm_layer == nn.InstanceNorm2d

        kw = 4
        padw = 1
        sequence = [
            nn.Conv2d(input_nc, 
                      ndf, 
                      kernel_size=kw, 
                      stride=2, 
                      padding=padw), 
            nn.LeakyReLU(0.2, True)
        ]
        
        nf_mult = 1
        nf_mult_prev = 1
        for n in range(1, n_layers):  # gradually increase the number of filters
            nf_mult_prev = nf_mult
            nf_mult = min(2 ** n, 8)
            sequence += [
                nn.Conv2d(ndf * nf_mult_prev, 
                          ndf * nf_mult, 
                          kernel_size=kw, 
                          stride=2, 
                          padding=padw, 
                          bias=use_bias),
                norm_layer(ndf * nf_mult),
                nn.LeakyReLU(0.2, True)
            ]

        nf_mult_prev = nf_mult
        nf_mult = min(2 ** n_layers, 8)
        sequence += [
            nn.Conv2d(ndf * nf_mult_prev, 
                      ndf * nf_mult, 
                      kernel_size=kw, 
                      stride=1, 
                      padding=padw, 
                      bias=use_bias),
            norm_layer(ndf * nf_mult),
            nn.LeakyReLU(0.2, True)
        ]
        
        # output 1 channel prediction map
        sequence += [nn.Conv2d(ndf * nf_mult, 
                               1, 
                               kernel_size=kw, 
                               stride=1, 
                               padding=padw)]
        self.model = nn.Sequential(*sequence)

    def forward(self, input):
        """Standard forward."""
        return self.model(input)