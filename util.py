import torch
import numpy as np
from skimage import img_as_ubyte
from torchvision.utils import make_grid


def tensor2im(input_image, imtype=np.uint8):
    """"Converts a Tensor array into a numpy image array.
    Parameters:
        input_image (tensor) --  the input image tensor array
        imtype (type)        --  the desired type of the converted numpy array
    """
    if not isinstance(input_image, np.ndarray):
        if isinstance(input_image, torch.Tensor):
            image_tensor = input_image.data
        else:
            return input_image
        if image_tensor.ndim == 3:
            image_numpy = image_tensor.cpu().float().numpy()
        else:
            image_numpy = image_tensor[0].cpu().float().numpy()
            
        if imtype==np.uint16:
            image_numpy = (image_numpy[0] + 1) / 2.0 * 65535.0  # for IF
            image_numpy = np.tile(image_numpy, (3, 1, 1))
        else:
#             image_numpy = (image_numpy + 1) / 2.0 * 255.0  # or HE
            image_numpy = (image_numpy + 1) / 2.0 * 255

    else:  # if it is a numpy array, do nothing
        image_numpy = input_image
    return image_numpy.astype(imtype)

def make_IF_grid(imgs):
    imgs = [make_grid(imgs[:,i,:,:].unsqueeze(1), nrow=1) for i in range(imgs.size(1))]
    imgs = torch.cat(imgs, axis=2)
    imgs = tensor2im(imgs, imtype=np.uint16)
    return img_as_ubyte(imgs)

def make_HE_grid(out):
    return tensor2im(make_grid(out,nrow=1))